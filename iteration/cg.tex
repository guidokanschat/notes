
\section{The conjugate gradient method}

\begin{todo}
  Distinguish between $V$ and $X$.
\end{todo}

\begin{intro}
  Relying on Hilbert space structure more than Richardson's iteration
  is the \putindex{conjugate gradient method} (cg), since it uses
  orthogonal search directions. Nevertheless, it also relies on
  constructing search directions from residuals, such that a
  \putindex{Riesz isomorphism} enters the same way as before and can
  then be used for preconditioning.
  
  The beauty of the conjugate gradient method is, that it is parameter
  and tuning free, and it converges considerably faster than a linear
  iteration method.
\end{intro}

\begin{Definition}{cg-method}
  Let $V$ be a Hilbert space and $V^*$ its dual. The \define{conjugate
  gradient method} for an iteration vector $u^{(k)} \in V$ involves the
  residuals $r^{(k)} \in V^*$ as well as the update direction $p^{(k)}
  \in V$ and the auxiliary vector $w^{(k)} \in V$. It consists of the
  steps
  \begin{enumerate}
  \item Initialization: for $f$ and $u^{(0)}$ given, compute
    \begin{xalignat*}{2}
      r^{(0)} &= f- a(u^{(0)},.) \\
      \scal(w^{(0)}, v) &= r^{(0)}(v) & \forall v &\in V \\
      p^{(0)} &= w^{(0)}.
    \end{xalignat*}
    \item Iteration step: for $u^{(k)}$, $r^{(k)}$, $w^{(k)}$, and
      $p^{(k)}$ given, compute
      \begin{xalignat*}2
        \alpha_k &= \frac{r^{(k)}(w^{(k)})}{a(p^{(k)},p^{(k)})} \\
        u^{(k+1)} &= u^{(k)} + \alpha_k p^{(k)} \\
        r^{(k+1)} &= r^{(k)} - \alpha_k a(p^{(k)},.) \\
      \scal(w^{(k+1)}, v) &= r^{(k+1)}(v) & \forall v &\in V \\
      \beta_k &= \frac{r^{(k+1)}(w^{(k+1)})}{r^{(k)}(w^{(k)})}\\
      p^{(k+1)} &= w^{(k+1)} + \beta_k p^{(k)}
      \end{xalignat*}
  \end{enumerate}
\end{Definition}

\begin{remark}
  The results on orthogonality and minimization properties of the cg
  method in~\cite{GrossmannRoosStynes07} or~\cite{Saad00} remain valid in this
  context. Differences occur in the interpretation of these
  properties. The conjugate gradient method does not necessarily
  converge in a finite number of steps, and if the bilinear form is
  unbounded, no convergence rate is guaranteed.
\end{remark}



% \begin{lemma}
%   Let $a(.,.)$ be symmetric and elliptic. Then, either $u^{(k)}$ is a
%   solution, or $u^{(k+1)}$ can be computed by a step of the conjugate
%   gradient method. Furthermore, there are the 
% \end{lemma}

\begin{Definition}{pcg-method}
  The \define{preconditioned cg method} is obtained from
  above algorithm by reinterpreting the \putindex{Riesz isomorphism}
  in the computation of $w^{(k+1)}$ as a preconditioning operation,
  much alike Definition~\ref{definition:richardson:2} of the
  preconditioned Richardson iteration. Thus, the line defining
  $w^{(k+1)}$ is replaced by
  \begin{xalignat*}2
    b(w^{(k+1)}, v) &= r^{(k+1)}(v) & \forall v &\in V .
  \end{xalignat*}
  Here, like there, the preconditioner enters naturally from the weak
  form of the algorithm.
\end{Definition}

\begin{Definition}{krylov-space}
  The $n$th \define{Krylov space} as subspace of the Hilbert space $V$
  with inner product $b(.,.)$ of the operator $A$ and seed vector
  $w \in V$ is
  \begin{gather}
    \label{eq:cg:2}
    \mathcal K_n = \mathcal K_n(B^{-1}A, w)
    = \operatorname{span}\left\{w, B^{-1}A w, (B^{-1}A)^2 w, \dots, (B^{-1}A w)^{n-1}\right\}.
  \end{gather}
\end{Definition}

\begin{Lemma}{cg-minimization}
  The iterates of the cg method have the following minimization
  properties:
  \begin{gather}
    \label{eq:cg:3}
    \begin{split}
      \norm{u^{(k)}-u}_A &= \min_{v\in \mathcal K_k} \norm{u^{(0)} + v -u}_A \\
      &= \min_{\substack{p\in P_{n-1}\\ p(0) = 1}}
      \norm{u^{(0)} + p(B^{-1}A) w -u}_A.
    \end{split}
  \end{gather}
\end{Lemma}

\begin{Theorem}{cg-convergence}
  Let the bilinear form $a(.,.)$ be symmetric, and let the
  \putindex{spectral equivalence}~\eqref{eq:richardson:12} hold. Then,
  the preconditioned cg method converges and we have the estimate
  \begin{gather}
    \label{eq:cg:1}
    \|u^{(k)} - u\|_A \le 2
    \left(\frac{\sqrt\kappa-1}{\sqrt\kappa+1}\right)^k \|u^{(0)} - u\|_A.
  \end{gather}
  Here, $\kappa = \Lambda/\lambda$ is the \putindex{spectral condition
    number} of the preconditioned problem.
\end{Theorem}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
