
\begin{remark}
  This part of the notes deals with preconditioning of symmetric
  operators, or those, which have a dominating symmetric part. The
  theory of preconditioning methods for nonsymmetric and in particular
  non-normal operators is currently not well developed and thus will not
  be covered by these notes. 
\end{remark}

\begin{notation}
  Iterative methods will be considered in a Hilbert space $X$ with
  inner product $\scal(.,.)$.
\end{notation}

\begin{intro}
  The motivation for the use of iterative methods lies in the fact
  that matrices resulting from the discretization of partial
  differential equations tend to be very big, but sparse, that is,
  most of their entries are zero. Let us take for example trilinear
  finite elements on a uniform grid of $(n-1)$ cells in each
  direction. This leeds to $n^3$ degrees of freedom which we number
  lexicographically. The matrix stencil, that is, the distribution of
  nonzero entries, in ome dimension is tridiagonal, that is,
  \begin{gather*}
    S_1 =
    \begin{pmatrix}
      * & * \\ * & * & * \\
      & \ddots & \ddots & \ddots \\
      && * & *
    \end{pmatrix}
  \end{gather*}
  In two dimensions, we obtain the stencil
  \begin{gather*}
    S_2 = S_1 \otimes S_1
    =
    \begin{pmatrix}
      S_1 & S_1 \\
      S_1 & S_1 & S_1 \\
      & \ddots & \ddots & \ddots \\
      &&S_1 & S_1
    \end{pmatrix},
  \end{gather*}
  and in three dimensions
  \begin{gather*}
    S_3 = S_1 \otimes S_1 \otimes S_1
    =
    \begin{pmatrix}
      S_2 & S_2 \\
      S_2 & S_2 & S_2 \\
      & \ddots & \ddots & \ddots \\
      &&S_2 & S_2      
    \end{pmatrix}.
  \end{gather*}
  The corresponding matrix has $N=n^3$ rows and columns. Let us compare
  some solution methods for $n=100$ with respect to memory and a
  hypothetic hardware which executes $10^9$ multiplications per second
  (additions are free).
  \begin{description}
  \item[Gaussian elimination] The effort needed is $\tfrac13
    N^3+\mathcal O(N^2)$,
    leading to the computing time
    \begin{gather*}
      T_{G} = \frac{10^{18}}{3\cdot 10^9} \;\text{sec}
      \approx 3\cdot10^8 \;\text{sec}
      \approx 10.6 \;\text{years}
    \end{gather*}
    The backward substitution is only of order $N^2$ and can be
    neglected. The memory requirement with double precision is
    $8\cdot10^{12}$ Bytes, almost 10 terabytes.
  \item[Banded LU decomposition] Here we make use of the fact that LU
    decomposition can be restricted to the hull of the outermost
    nonzero elements of the matrix, the so called banded or skyline
    version. Let $M$ be the greatest distance of a nonzero matrix
    element $a_{ij}$ from the diagonal, that is, $M=\left|i-j\right|$. Then,
    the leading term of the effort is $\tfrac13 N\cdot M^2$, yielding
    with $M=n^2$ a computing time of
      \begin{gather*}
        T_{BLU} = \frac{10^{6}\cdot 10^{4} \cdot 10^{4}}{3\cdot 10^9} \;\text{sec}
      \approx 3\cdot10^4 \;\text{sec}
      \approx 9 \;\text{hours}
      \end{gather*}
      The storage requirement for $N\cdot M$ double precision numbers
      is $8\cdot 10^{10}$ Bytes, almost 100 gigabytes.
    \item[matrix vector product] For comparison, the multiplication
      with such a matrix, given that there are at most 9 nonzero
      entries per row costs
      \begin{gather*}
        T_{mult} = \frac{9\cdot 10^6}{10^9} \approx 0.01 \;\text{sec},
      \end{gather*}
      that is, we can perform more than $10^6$ steps of an iterative
      method before we reach the effort of the banded LU
      decomposition. The storage requirement is roughly 1 gigabyte and
      can be reduced to almost zero by a smart implementation. 
  \end{description}
\end{intro}

\begin{remark}
  For purposes of analysis we typically choose the space $X =
  L^2(\Omega)$. We admit a small inaccuracy here: when we run the
  algorithms on a computer, we usually employ the Eiclidean inner
  product, thus $X$ should be the space of degrees of freedom. But
  this is a discrete space, where we cannot use theory of function
  spaces easily. Instead, we note that the $L^2$-inner product of
  standard finite element bases yield inner products equivalent to the
  Euclidean up to the local mesh size (see Lemma~\ref{lemma:itintro:1}).
\end{remark}

\begin{example}
  While the methods developed in this chapter are fairly general, we
  introduce a specific model problem as a simple benchmark case. To
  this end, we consider the Dirichlet problem: find $u\in V =
  H^1_0(\Omega)$ such that
  \begin{gather}
    \label{eq:itintro:1}
    a(u,v) \equiv \int_\Omega \nabla u\cdot \nabla v \dx
    = \int_\Omega f v \dx \equiv f(v),
    \qquad \forall v\in V.
  \end{gather}
  The finite dimensional linear systems of equations are derived from
  finite element discretizations on quasi-uniform meshes of cells with
  maximal diameter $h$, yielding a sequence of spaces $V_h$, on which
  linear systems are introduced by the same weak
  form~\eqref{eq:itintro:1}.
\end{example}

\begin{notation}
  With a bilinear form $a(.,.)$ on $X\times X$ we associate the
  operator $A: X\to X$ by
  \begin{gather}
    \label{eq:itintro:2}
    \scal(Au,v) = a(u,v), \quad \forall v\in V,
  \end{gather}
  where now $V = \mathcal D(A)$ is the \define{domain} of $A$, that is, the
  subset of functions $v\in X$, such that $Av$ is defined and in $X$.
  
  We will tacitly assume that operators $A$, $B$, etc.\ are defined by
  equation~\eqref{eq:itintro:2} and the bilinear forms $a(.,.)$,
  $b(.,.)$, etc., respectively, if they are not defined otherwise.
\end{notation}

\begin{Definition}{bilinear-properties}
  We call the bilinear form $a(.,.)$ and its associated operator $A$
  \define{symmetric}, if there holds
  \begin{gather*}
    a(u,v) = a(v,u) \qquad \forall u,v \in V.
  \end{gather*}
  They are called $V$-\define{elliptic}, if for there is a positive number
  $\gamma$ such that
  \begin{gather*}
    a(u,u) \ge \gamma \norm{u}_V^2 \qquad \forall u\in V.
  \end{gather*}
\end{Definition}

\begin{Definition}{spd-condition-number}
  \defindex{Lambdaa@$\Lambda(A)$}
  \defindex{lambdaa@$\lambda(A)$}
  For positive definite, symmetric operators, we obtain the possibly
  infinite bounds of the spectrum
  \begin{gather}
    \label{eq:richardson:8}
    \Lambda(A) = \sup_{u\in V}\frac{a(u,u)}{\norm{u}_X^2},
    \qquad
    \lambda(A) = \inf_{u\in V}\frac{a(u,u)}{\norm{u}_X^2},
  \end{gather}
  \defindex{kappaa@$\kappa(A)$}
  \index{condition number|see {spectral condition number}}
  as well as the possibly infinite \define{spectral condition number}
  \begin{gather*}
   \kappa(A) = \frac{\Lambda(A)}{\lambda(A)}.
  \end{gather*}
\end{Definition}

\begin{remark}
  Note that the spectral condition number depends on the norm of the
  space $X$. It is bounded, if and only if $A$ is bounded with respect
  to this norm.
\end{remark}

\begin{example}
  Let $X=H^1_0(\Omega)$ with the inner product
  \begin{gather*}
    \scal(u,v)_1 = \int_\Omega \nabla u\cdot \nabla v \dx.  
  \end{gather*}
  If $A$ is the operator associated with the bilinear form $a(.,.)$
  in~\eqref{eq:itintro:1}, then 
  \begin{gather*}
    \Lambda(A) = \lambda(A) = \kappa(A) = 1.    
  \end{gather*}
  If on the other hand $X = L^2(\Omega)$ equipped with the usual
  $L^2$-inner product, then $A$ is unbounded and thus $\kappa(A) =
  \infty$. $\lambda(A)$ is the constant in Friedrichs' inequality.
\end{example}

\begin{notation}
  After choosing a basis for a finite dimensional space $X_n$ or a
  Schauder basis for the space $X$ (assuming $X$ separable), say
  $\{\phi_i\}$, we can define a (possibly infinite-dimensional) matrix
  $\mat A$ associated with the bilinear form $a(.,.)$ with the entries
  \begin{gather*}
    a_{i j} = a(\phi_j, \phi_i).
  \end{gather*}
  
  If we restrict the bilinear forms to a finite dimensional subspace
  $X_n$, we denote the matrices $\mat A$ restricted to this subspace
  by $\mat A_n$.
\end{notation}

\begin{Definition}{matrix-condition-number}
  \defindex{Lambdama@$\Lambda(\mat A)$}
  \defindex{lambdama@$\lambda(\mat A)$}
  The two extremal eigenvalues of the matrix $\mat A_n$ can be
  obtained by the maximum and minimum of the \define{Rayleigh
    quotient}
  \begin{gather}
    \label{eq:itintro:3}
    \Lambda(\mat A) = \max_{\vec x\in \R^n}\frac{\vec x^T \mat A \vec x}{\vec x^T\vec x},
    \qquad
    \lambda(\mat A) = \min_{\vec x\in \R^n}\frac{\vec x^T \mat A \vec x}{\vec x^T\vec x}.
  \end{gather}
  \defindex{kappama@$\kappa(\mat A)$}
  The \putindex{spectral condition number} is
  \begin{gather*}
    \kappa_n(\mat A) = \frac{\Lambda(\mat A)}{\lambda(\mat A)}.
  \end{gather*}
\end{Definition}

\begin{remark}
  The spectral condition number of the operator $A$ depends on the
  bilinear form $a(.,.)$ and the choice of the norm in $X$. On the
  other hand, the spectral condition number of the matrix $\mat A$
  depends on the choice of a basis of the space $X_n$.
\end{remark}

\begin{Lemma}{condition-number-mass-matrix}
  \label{lemma:itintro:1}
  Let $\{\phi_i\}$ be the standard, piecewise linear, finite element
  basis on a quasi-uniform triangulation of mesh size $h$. Let $\mat
  M$, the so called \define{mass matrix} be the matrix associated with
  the $L^2$-inner product with entries
  \begin{gather*}
    m_{ij} = \int_\Omega \phi_i(x) \phi_j(x) \dx.
  \end{gather*}
  Then,
  \begin{gather*}
    \Lambda(\mat M) \simeq h^{d} \simeq  \lambda(\mat M)
  \end{gather*}
  Therefore, the condition number is
  \begin{gather*}
    \kappa(\mat M) = \frac{\mathcal O(h^{d})}{\mathcal O(h^{d})} = {\mathcal O(1)}.
  \end{gather*}
\end{Lemma}

\begin{proof}
  It is easy to verify, that $m_{ii}> 0$, and that not more
  entries in each row as edges of the triangulation meet in one vertex
  are different from zero. Furthermore, that the size of those entries
  is of order $h^d$, where $d$ is the space dimension. From these two
  facts we immediately obtain
  \begin{gather*}
    \Lambda(\mat M) = \mathcal O(h^{d}).
  \end{gather*}
  The argument for $\lambda(\mat M)$ is more subtle. For any mesh cell
  $T$, let $\vec x_T$ be the entries of the vector $\vec x$ which
  belong to node values of the cell $T$. Let $\mat M_T$ be the cell
  mass matrix obtained by restricting the $L^2$-inner product to
  $T$. Then,
  \begin{gather*}
    \vec x^T \mat M \vec x
    = \sum_{T\in\T_h} \vec x^T_T \mat M_T \vec x_T
    \ge \min_{T\in\T_h} \frac{\vec x^T_T \mat M_T \vec x_T}{\vec
      x^T_T\vec x_T}
    \sum_{T\in\T_h} |\vec x_T|^2 \ge \lambda(\mat M_T)  |\vec x|^2.
  \end{gather*}
  In order to estimate the eigenvalues of $\mat M_T$, we note that for
  a unisolvent element, the norms $|\vec x_T|$ and $\|u\|_{0,T}$ are
  equivalent on the reference cell, and the $L^2$-norm scales with
  $h^d$ when transforming to the real cell $T$. Thus, we have
  $\lambda(\mat M) = \mathcal O(h^{d})$.
\end{proof}

\begin{Corollary}{refined-condition-number}
  Let $\mesh_h$ be a mesh with cell sizes ranging between the minimum $h$ and
  the maximum $H$. Then, we have
  \begin{align*}
    \Lambda(\mat M) &= \mathcal O(H^{d}) \\
    \lambda(\mat M) &= \mathcal O(h^{d}) \\
    \kappa(\mat M) &= \mathcal O\left(\left(\frac Hh\right)^{d}\right)
  \end{align*}
\end{Corollary}

\begin{todo}
  This can be fixed by using weighted norms in $\R^n$.
\end{todo}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
